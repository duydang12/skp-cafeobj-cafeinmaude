-- in ../SKP
-- use inv4
-- ---------- inv3 property ------------

-- (I) Base case
open INV .
:id(INV)
-- fresh constants
ops i j : -> NzNat .
-- check
eq j = 1 .
eq i = 1 .
red inv3(init,i,j) .
close

open INV .
:id(INV)
-- fresh constants
ops i j : -> NzNat .
-- check
eq j = 1 .
eq (i = 1) = false .
red inv3(init,i,j) .
close

open INV .
:id(INV)
-- fresh constants
ops i j : -> NzNat .
-- check
eq (j = 1) = false .
red inv3(init,i,j) .
close

-- (II) Induction case
-- rem
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(try(s,p),i,j) .
close

-- l1
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(setReq(s,p),i,j) .
close

-- l2

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(checkPriv(s,p),i,j) .
close

-- l3
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(incReqNo(s,p),i,j) .
close

-- l4
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(sendReq(s,p),i,j) .
close

-- l5
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (pc(s,p) = l5) = false .
-- check
red inv3(s,i,j) implies inv3(waitPriv(s,p,pri),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l5 .
eq (msg(p,pri) \in nw(s)) = false .
-- check
red inv3(s,i,j) implies inv3(waitPriv(s,p,pri),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l5 .
eq (msg(p,pri) \in nw(s)) = true .
eq j = p .
eq i = p .
-- check
red inv3(s,i,j) implies inv3(waitPriv(s,p,pri),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l5 .
eq (msg(p,pri) \in nw(s)) = true .
eq j = p .
eq (i = p) = false .
-- check
red inv3(s,i,j) implies inv4(s,p,i,pri) implies inv3(waitPriv(s,p,pri),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l5 .
eq (msg(p,pri) \in nw(s)) = true .
eq (j = p) = false .
eq (i = p) = false .
-- check
red inv3(s,i,j) implies inv3(waitPriv(s,p,pri),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op pri : -> Privilege .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l5 .
eq (msg(p,pri) \in nw(s)) = true .
eq (j = p) = false .
eq i = p .
-- check
red inv3(s,i,j) implies inv4(s,p,j,pri) implies inv3(waitPriv(s,p,pri),i,j) .
close

-- cs
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(exit(s,p),i,j) .
close

-- l6
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(completeReq(s,p),i,j) .
close

-- l7
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(updateQueue(s,p),i,j) .
close

-- l8
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(checkQueue(s,p),i,j) .
close

-- l9
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (pc(s,p) = l9) = false .
-- check
red inv3(s,i,j) implies inv3(transferPriv(s,p),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq pc(s,p) = l9 .
-- check
red inv3(s,i,j) implies inv3(transferPriv(s,p),i,j) .
close

-- l10
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
-- check
red inv3(s,i,j) implies inv3(resetReq(s,p),i,j) .
close

-- receiveReq
open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = false .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq node(re) = p .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq j = p .
eq havePriv(s,p) = false .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq j = p .
eq havePriv(s,p) = true .
eq i = p .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq j = p .
eq havePriv(s,p) = true .
eq (i = p) = false .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq (j = p) = false .
eq havePriv(s,j) = true .
eq (i = p) = false .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq (j = p) = false .
eq havePriv(s,j) = true .
eq i = p .
eq havePriv(s,p) = false .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq (j = p) = false .
eq havePriv(s,j) = true .
eq i = p .
eq havePriv(s,p) = true .
-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close

open INV .
:id(INV)
-- fresh constants
op s : -> Sys .
ops i j p : -> NzNat .
op re : -> Request .
-- induction hypothesis
eq [:nonexec] : inv3(s,I:NzNat,J:NzNat) = true .
-- assumptions
eq (msg(p,re) \in nw(s)) = true .
eq (node(re) = p) = false .
eq (j = p) = false .
eq havePriv(s,j) = false .

-- check
red inv3(s,i,j) implies inv3(receiveReq(s,p,re),i,j) .
close


